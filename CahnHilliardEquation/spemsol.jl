# include libraries
include("alltypes.jl")
include("prepare.jl")
include("simulate.jl")
include("write.jl")

# prepare coordinates for physical system
thetaArr, phiArr = prepareCoordinates(lmax)
# prepare array of initial conditions
c = prepareInitialFunction(thetaArr, phiArr, c0, eps)
# launch simulation
launchSimulation(c, thetaArr, phiArr, tmax, tstep, ca, cb, rho, kappa, M, R)
