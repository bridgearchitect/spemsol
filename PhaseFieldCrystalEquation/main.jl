using FastSphericalHarmonics

# define constants 
lmax = 100
r = 0.0
R = 1.0
tmax = 1.0
tstep = 0.0005
period_print = 100
small_number = 0.0000000001
# define string constants
filename = "results"

# function to prepare array of coordinates
function prepareCoordinates(lmax)

    # create array for coordainates
    thetaArr, phiArr = sph_points(lmax + 1)
    # return created arrays
    return thetaArr, phiArr

end

# function to calculate array of initial function
function prepareInitialFunction(thetaArr, phiArr)

    # calculate values of function
    f = [rand() for thetaI in thetaArr,
    phiI in phiArr]
    # return created array
    return f

end

# function to calculate spherical harmonic transformation
function calculateSphTransormation(f)

    # calculate harmonic transformation
    f_harm = sph_transform(f)
    # return calculated array
    return f_harm

end

# function to calculate the third power of original function on sphere
function calculateThirdPower(f_orig, thetaArr, phiArr)

    # copy the received array 
    f3_orig = deepcopy(f_orig)
    # write data about received function
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            f3_orig[i,j] = f_orig[i,j] * f_orig[i,j] * f_orig[i,j]
            j = j + 1
        end
        i = i + 1
    end
    # return calculated array
    return f3_orig

end

# function to calculate time evolution
function calculateTimeEvolution(f_harm, f3_harm, tstep, r, R, lmax)

    # copy previous sperical harmonic representation
    fnew_harm = deepcopy(f_harm)
    # go through coefficients
    for l in 0:lmax
        for m in -l:l
            # calculate alhpa coefficient
            alpha_l = 1.0 + tstep * l * (l + 1) * (r + (1.0 - l *
                (l + 1.0) / R^2)^2) / R^2
            # calculate new value for original
            fnew_harm[sph_mode(l,m)] = (1.0 / alpha_l) *
            (f_harm[sph_mode(l,m)] - tstep * l *
            (l + 1.0) * f3_harm[sph_mode(l,m)] / R^2)
        end
    end
    # return calculated new array
    return fnew_harm

end

# function to launch simulation for phase field equation
function launchSimulation(f, thetaArr, phiArr, tmax, tstep, r, R, lmax)

    # initialization of variables
    tcur = 0.0
    i = 0
    # calculate spherical harmonic transformation for initial function
    f_harm = calculateSphTransormation(f)
    # calculate inversed spherical harmonic transformation
    f_orig = sph_evaluate(f_harm)
    # write the first step
    writeAnswer(f_orig, thetaArr, phiArr, i)
    
    # go to the next step
    tcur = tcur + tstep
    i = i + 1
    # go through time domain
    while tcur <= tmax
        # calculate the third power for original function
        f3_orig = calculateThirdPower(f_orig, thetaArr, phiArr)
        # calculate spherical harmonic image for the third power function
        f3_harm = calculateSphTransormation(f3_orig)
        # calculate time evolution of physical system
        f_harm = calculateTimeEvolution(f_harm, f3_harm, tstep,
                                        r, R, lmax)
        # calculate inversed spherical harmonic transformation
        f_orig = sph_evaluate(f_harm)
        # write answer it it is essential
        if (i % period_print == 0) || (abs(tcur - tmax) < small_number)
            writeAnswer(f_orig, thetaArr, phiArr, i)
        end
        # go to the next step
        tcur = tcur + tstep
        i = i + 1
    end

end

# function to write answer
function writeAnswer(f, thetaArr, phiArr, iter)

    # create file
    file = open(filename * string(iter) * ".dat", "w")
    # write header file
    write(file, "# theta phi f\n")
    
    # write data about received function
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            thetastr = string(thetaI)
            phistr = string(phiI)
            fstr = string(f[i,j])
            write(file, thetastr * " " * phistr * " " * fstr * "\n")
            j = j + 1
        end
        i = i + 1
    end

    # close file
    close(file)

end

# prepare coordinates for physical system
thetaArr, phiArr = prepareCoordinates(lmax)
# prepare array of initial conditions
f = prepareInitialFunction(thetaArr, phiArr)
# launch simulation
launchSimulation(f, thetaArr, phiArr, tmax, tstep, r, R, lmax)
