# define solver constants
lmax = 100
tmax = 100000.0
c0 = 0.5
eps = 0.05
tstep = 0.01
snapshotPeriod = 100000
paramPeriod = 10000
smallNumber = 0.00000000001

# define physical constants
ca = 0.3
cb = 0.7
rho = 5.0
M = 5.0
kappa = 2.0
R = 100.0

# define math constant
q = 2.5

# define string constants
filenameSnapshot = "result"
filenameParam = "parameters"
