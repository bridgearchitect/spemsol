# spemsol

spemsol (spectral method solver) is the special software to solve partial 
differential equations using spectral methods (Fourier series or spherical harmonics).

The given project has the following folders:

1. KDVEquation contains project to solve numerically 1D KDV equation
using Fourier representation (Fast Fourier transformation).
2. SphericalHarmonicTransformation is simple project to demonstrate
possibility to work with spherical harmonics in Julia programming
language.
3. PhaseFieldCrystalEquation is the project to solve phase field crystal
equation on unit sphere using spherical harmonics representation 
(Gauss-Legendre transformation).
4. CahnHilliardEquation is the project to solve Cahn-Hilliard equation
on unit sphere using spherical harmonics in Julia programming language.
