include("alltypes.jl")

# function to prepare array of coordinates
function prepareCoordinates(lmax)

    # create array for coordainates
    thetaArr, phiArr = sph_points(lmax + 1)
    # return created arrays
    return thetaArr, phiArr

end

# function to calculate array of initial function
function prepareInitialFunction(thetaArr, phiArr, c0, eps)

    # calculate values of function
    c = [c0 + eps * (cos(8.0 * thetaI) * cos(15.0 * phiI) +
        (cos(12.0 * thetaI) * cos(10.0 * phiI))^2 + cos(2.5 * thetaI - 1.5 * phiI) *
        cos(7.0 * thetaI - 2.0 * phiI)) for thetaI in thetaArr,
        phiI in phiArr]
    # return created array
    return c

end
