# include modules
include("alltypes.jl")

# function to write header in parametric file
function writeHeaderParametricFile()

    # open file
    file = open(filenameParam * ".txt", "w")
    # write header in file
    write(file, "# t m E\n")
    # close file
    close(file)

end

# function to write new global values in parametric file
function writeGlobalValuesParametricFile(t, mass, energy)

    # open file
    file = open(filenameParam * ".txt", "a")
    # write new row of information (global values) in file
    write(file, string(t) * " " * string(mass) * " " * string(energy) * "\n")
    # close file
    close(file)

end

# function to write snaptshot of system
function writeSnapshotSystem(c, thetaArr, phiArr, iter)

    # create array to store coordinates
    x = zeros(size(c))
    y = zeros(size(c))
    z = zeros(size(c))

    # go through all angles
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            # transform spherical coordinates into Cartesius coordinates
            x[i,j] = sin(thetaI) * cos(phiI)
            y[i,j] = sin(thetaI) * sin(phiI)
            z[i,j] = cos(thetaI)
            j = j + 1
        end
        i = i + 1
    end

    # open file
    file = open(filenameSnapshot * string(iter) * ".txt", "w")
    # find number of angles
    numTheta = size(thetaArr, 1)
    numPhi = size(phiArr, 1)
    # write the first row
    write(file, string(numTheta) * " " * string(numPhi) * "\n")

    # go through all coordinates
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            # convert numbers into strings
            xstr = string(x[i,j])
            ystr = string(y[i,j])
            zstr = string(z[i,j])
            cstr = string(c[i,j])
            # write new information
            write(file, xstr * " " * ystr * " " * zstr * " " * cstr * " ")
            j = j + 1
        end
        # finish the given row
        write(file, "\n")
        i = i + 1
    end

    # close file
    close(file)

end
