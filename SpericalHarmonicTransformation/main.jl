using FastSphericalHarmonics

# define constants 
lmax = 4

# receive points for spherical harmonics transformation
thetaArr, phiArr = sph_points(lmax + 1)
# print angle values for points in sphere
println("Angle values:") 
for thetaI in thetaArr
    for phiI in phiArr
        print(string(thetaI) * " " * string(phiI) * "\n")
    end
end

# calculate function for transformation
F = [cos(thetaI) + sin(thetaI) * cos(phiI) for thetaI in thetaArr, phiI in phiArr]
# print values of function for transformation
println("Values of initial function:")
println(F)

# calculate spherical harmonics transformation
C = sph_transform(F)
# print values of spherical harmonics transformation
println("Values of spherical harmonics transformation:")
println(C)

# calculate inversed spherical harmonics transformation
Ftran = sph_evaluate(C)
# print values of inversed spherical harmonics transformation
println("Values of inversed spherical harmonics transformation:")
println(Ftran)
