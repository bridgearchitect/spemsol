# include modules
using FastSphericalHarmonics
include("alltypes.jl")
include("write.jl")

# define generic function for integration using trapezoidal approach
function genMathFunction(theta, phi, value)

    # calculate value of generic function
    g = (sin(theta))^(2 * q - 1) * (q * (cos(theta))^2 + (sin(theta))^2) * value /
        ((sin(theta))^(2 * q) + (cos(theta))^2)^1.5
    # return calculated value
    return g

end

# function to calculate spherical harmonic transformation
function calculateSphTransormation(c)

    # calculate harmonic transformation
    c_harm = sph_transform(c)
    # return calculated array
    return c_harm

end

# function to calculate nonlinear function using original function
function calculateNonlinearFunction(c_orig, thetaArr, phiArr, rho, ca, cb)

    # create array for nonlinear function
    f_orig = zeros(size(c_orig))

    # go through all coefficients
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            f_orig[i,j] = 2.0 * rho * (ca - c_orig[i,j]) * (-cb + c_orig[i,j]) * (ca + cb - 2.0 * c_orig[i,j])
            j = j + 1
        end
        i = i + 1
    end

    # return calculated array
    return f_orig

end

# function to calculate mass of physical system using original function
function calculateMassSystem(c_orig, thetaArr, phiArr, R)

    # initialization
    mass = 0.0
    dTheta = thetaArr[2] - thetaArr[1]
    dPhi = phiArr[2] - phiArr[1]

    # go through all points
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiJ in phiArr
            # calculate new portion of mass
            mass += dTheta * dPhi * R^2 * genMathFunction(thetaI, phiJ, c_orig[i,j])
            j = j + 1
        end
        i = i + 1
    end

    # return value of mass
    return mass

end

# function to calculate energy of physical system using original function
function calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho, ca, cb)

    # initialization
    energy = 0.0
    dTheta = thetaArr[2] - thetaArr[1]
    dPhi = phiArr[2] - phiArr[1]
    # define number of points along two angles
    nTheta = size(thetaArr, 1)
    nPhi = size(phiArr, 1)

    # go through all points
    for i in 1:nTheta
        for j in 1:nPhi
            # initialization
            theta = thetaArr[i]
            phi = phiArr[j]
            c = c_orig[i, j]
            # calculate chemical energy
            fchem = rho * (c - ca)^2 * (c - cb)^2
            # calculate gradient along theta angle
            gradTheta = 0.0
            if i != nTheta
                cp = c_orig[i + 1, j]
                gradTheta = (1.0 / R) * (cp - c) / dTheta
            else
                cm = c_orig[i - 1, j]
                gradTheta = (1.0 / R) * (c - cm) / dTheta
            end
            # calculate gradient along phi angle
            gradPhi = 0.0
            if j != nPhi
                cp = c_orig[i, j + 1]
                gradPhi = (1.0 / (R * sin(theta))) * (cp - c) / dPhi
            else
                cm = c_orig[i, j - 1]
                gradPhi = (1.0 / (R * sin(theta))) * (c - cm) / dPhi
            end
            # calculate square of the length for gradient
            sqGrad = gradTheta * gradTheta + gradPhi * gradPhi
            # calculate new portion of energy
            energy += dTheta * dPhi * R^2 * genMathFunction(theta, phi, fchem + kappa * sqGrad / 2.0)
        end
    end

    # return value of mass
    return energy

end

# function to calculate time evolution using transformed function in spherical harmonics
function calculateTimeEvolution(c_harm, f_harm, tstep, kappa, M, R, thetaArr, phiArr)

    # create array for new values of spherical harmonic representation
    cnew_harm = zeros(size(c_harm))
    # calculate Laplace differential operator for spherical harmonic representation
    lapl_c_harm = sph_laplace(c_harm)
    lapl2_c_harm = sph_laplace(lapl_c_harm)
    lapl_f_harm = sph_laplace(f_harm)

    # go through all coefficients
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            # normalize spherical harmonics for the specified sphere
            lapl_c_harm[i,j] = lapl_c_harm[i,j] / (R * R)
            lapl2_c_harm[i,j] = lapl2_c_harm[i,j] / (R * R * R * R)
            lapl_f_harm[i,j] = lapl_f_harm[i,j] / (R * R)
            j = j + 1
        end
        i = i + 1
    end

    # go through all coefficients
    i = 1
    for thetaI in thetaArr
        j = 1
        for phiI in phiArr
            # calculate new value for original
            cnew_harm[i,j] = c_harm[i,j] + M * tstep * lapl_f_harm[i,j] - kappa *
                tstep * lapl2_c_harm[i,j]
            j = j + 1
        end
        i = i + 1
    end

    # return calculated new array
    return cnew_harm

end

# function to launch simulation for phase field equation
function launchSimulation(c, thetaArr, phiArr, tmax, tstep, ca, cb, rho, kappa, M, R)

    # initialization of variables
    tcur = 0.0
    i = 0

    # calculate spherical harmonic transformation for initial function
    c_harm = calculateSphTransormation(c)
    # calculate inversed spherical harmonic transformation
    c_orig = sph_evaluate(c_harm)

    # calculate initial mass and energy of system
    mass = calculateMassSystem(c_orig, thetaArr, phiArr, R)
    energy = calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho, ca, cb)

    # write the first snapshot in system
    writeSnapshotSystem(c_orig, thetaArr, phiArr, i)
    # write header row in parametric file
    writeHeaderParametricFile()
    # write initial global values in parametric file
    writeGlobalValuesParametricFile(tcur, mass, energy)

    # go to the next step
    tcur = tcur + tstep
    i = i + 1
    # go through time domain
    while tcur <= tmax
        # calculate non-linear function f using original function
        f_orig = calculateNonlinearFunction(c_orig, thetaArr, phiArr, rho, ca, cb)
        # calculate spherical harmonic image for non-linear function f
        f_harm = calculateSphTransormation(f_orig)
        # calculate time evolution of physical system
        c_harm = calculateTimeEvolution(c_harm, f_harm, tstep, kappa, M, R, thetaArr, phiArr)
        # calculate inversed spherical harmonic transformation
        c_orig = sph_evaluate(c_harm)
        # write snapshot (state of system) it it is essential
        if (i % snapshotPeriod == 0) || (abs(tcur - tmax) < smallNumber)
            writeSnapshotSystem(c_orig, thetaArr, phiArr, i)
        end
        # write initial global values in parametric file it it is essential
        if (i % paramPeriod == 0) || (abs(tcur - tmax) < smallNumber)
            # calculate energy and mass of system
            mass = calculateMassSystem(c_orig, thetaArr, phiArr, R)
            energy = calculateEnergySystem(c_orig, thetaArr, phiArr, R, kappa, rho, ca, cb)
            # write energy and mast of system in parametric file
            writeGlobalValuesParametricFile(tcur, mass, energy)
        end
        # go to the next step
        tcur = tcur + tstep
        i = i + 1
    end

end
