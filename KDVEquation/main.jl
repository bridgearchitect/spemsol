using FFTW

# define constants
N = 1024
tmax = 1.0
tstep = 0.000005
min_value = -10.0
length = 20.0
filename = "results"
period_print = 2000

# function to prepare array of coordinates
function prepareCoordinates(min_value, length, N)

	# create array for coordainates
	coord = Array{Float64}(undef, N)
	for i in 1:N
		coord[i] = min_value + length * (i - 1) / N
	end

	# return created array
	return coord

end

# function to prepare array of frequency
function prepareFrequency(delta_freq, N)

	# create array for frequency
	freq = Array{Float64}(undef, N)
	for i in 1:N
		if i <= N/2 + 1
			freq[i] = (i - 1) * delta_freq
		else
			freq[i] = (i - 1 - N) * delta_freq
		end
	end

	# return created array
	return freq

end

# function to calculate array of initial function
function prepareInitialFunction(coord, N)

	# calculate values of function
	f = Array{Float64}(undef, N)
	for i in 1:N
		f[i] = 8.0 * sech(2.0 * (coord[i] + 8.0)) * sech(2.0 * (coord[i] + 8.0)) + 
		2.0 * sech(coord[i] + 1.0) * sech(coord[i] + 1.0) 
	end

	# return created array
	return f

end

# function to convert frequency represenation to coordinate represenation
function convertFrequencyToCoordinate(u_freq, N)

	# apply FFT transformation
	u_com = ifft(u_freq)
	# save only real part
	u = Array{Float64}(undef, N)
	for i in 1:N
		u[i] = real(u_com[i])
	end

	# return created function
	return u

end


# function to solve general KdV equation for the given step
function solveEquation(u_freq, freq, tstep, N)

	# solve linear part
	for i in 1:N
		u_freq[i] = u_freq[i] * exp(im * tstep * freq[i] * freq[i] * freq[i])
	end

	# return to basic function via inversed Fourier transformation
	u = ifft(u_freq)
	# conduct intermediate calculation for square of basic function
	usqr = Array{Float64}(undef, N)
	for i in 1:N
		usqr[i] = real(u[i]) * real(u[i])
	end
	# calculate FFT for square of basic function
	usqr_freq = fft(usqr)

	# solve non-linear part
	for i in 1:N
		u_freq[i] = u_freq[i] - tstep * 3.0 * im * freq[i] * usqr_freq[i]
	end

end

# function to write answer
function writeAnswer(u, coord, iter, N)

	# create file
	file = open(filename * string(iter) * ".txt", "w")
	# write header file
	write(file, "# x u\n")

	# write data about received function
	for i in 1:N
		xstr = string(coord[i])
		ustr = string(u[i])
		write(file, xstr * " " * ustr * "\n")
	end

	# close file
	close(file)

end

# function to launch simulation
function launchSimulation(u, freq, coord, tmax, N)

	# initialization of variables
	tcur = 0.0
	i = 0

	# calculate Fourier transformation for initial function
	u_freq = fft(u)
	# write the first step
	writeAnswer(u, coord, i, N)

	# go to the next step
	tcur = tcur + tstep
	i = i + 1
	# go through time domain
	while tcur <= tmax
		# function to solve KdV equation for the given step
		solveEquation(u_freq, freq, tstep, N)
		# convert frequency representation to coordinate representation
		u = convertFrequencyToCoordinate(u_freq, N)
		# write answer it it is essential
		if i % period_print == 0
			writeAnswer(u, coord, i, N)
		end
		# go to the next step
		tcur = tcur + tstep
		i = i + 1
	end

end

# prepare array of coordinates
coord = prepareCoordinates(min_value, length, N)
# calculate step of coordianate and frequence grids
delta_coord = coord[2] - coord[1]
delta_freq = 2.0 * pi / (N * delta_coord)
# prepare array of frequency
freq = prepareFrequency(delta_freq, N)
# prepare array of initial values
f = prepareInitialFunction(coord, N)
# launch numerical simulation
launchSimulation(f, freq, coord, tmax, N)